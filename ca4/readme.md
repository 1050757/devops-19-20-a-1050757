Ca4 - Docker - Containers with docker 

### Introduction to Containers and Docker

A **container** is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another.

In comparison, with virtual machines: 
* Don´t need an individual operating system, in each of which isolates memory, filesystem and network resources.
* They are significantly more lightweight that virtual machines.
* Create isolated environments for applications, while sharing a single operating system.

A **container**:
* Help create and share standardized environments, with a simple text file to set up.
* Are useful when our application has non-trivial dependencies.
* Are well suited to support microservices.

So, what is **docker**?

Docker is an open source platform in the Go programming language, which has high performance and is developed directly at Google.
It groups together pieces of software from a complete file system that covers all the resources necessary for its execution.
So, it is a container platform.

##### Docker image and Docker container

A **Docker image** is an immutable (unchangeable) file that contains the source code, libraries, dependencies, tools, and other files needed for an application to run.
Due to their read-only quality, these images are sometimes referred to as snapshots. 

A **Docker container** is a virtualized run-time environment where users can isolate applications from the underlying system. These containers are compact, portable units in which you can start up an application quickly and easily.

![docker](DockerContainers1.jpg)

Docker Compose provides a way to orchestrate multiple containers that work together. Examples include a service that processes requests to a db and a web, like we are going to developed in this project.

Docker-compose works from a  YAML file, that defines all the services to be deployed. These services rely on either a DockerFile or an existing container image.


A Docker Compose file enables the management of all the stages in a defined service's life cycle: starting, stopping, and rebuilding services; viewing the service status; and log streaming.

### Task 4: Development

First, I install Docker in you computer, I used docker Toolbox, because of my operating system windows 10 Home, do not support virtualization with Hyper-V.
Either way I need to have installed VirtualBox to run docker.

Now I need to developed my docker files to build the docker image to run the service web - **TOMCAT**, and docker image to run the database server - **H2**.

#### Service Web

* First I mentioned, that will be build from an image of tomcat, then install all the dependencies(ex: git,node,nmp,). 

    ```` From tomcat````

    ```` RUN apt-get update -y````

    ````RUN apt-get install -f````

    ````RUN apt-get install git -y````

    ````RUN apt-get install nodejs -y````

    ````RUN apt-get install npm -y````
    
* Then create a directory /tmp/build that will be used to clone my repository.

    ````RUN mkdir -p /tmp/build````

    ````WORKDIR /tmp/build/````
    
* I give all the permissions needed, clone the repository and move to the folder where I have my previous project that will be used (Ca3 - Part 2).
    
    ````RUN git clone https://1050757@bitbucket.org/1050757/devops-19-20-a-1050757.git````

    ````RUN chmod -R 777 /tmp/build/devops-19-20-a-1050757````

    ````WORKDIR /tmp/build/devops-19-20-a-1050757/ca3/part2/demo````
    
* In my case, I need to deleted node, and node_modules, remove build folder, and then build my project.
    
    ````RUN rm -rf node````

    ````RUN rm -rf node_modules````

    ````RUN ./gradlew clean build````
    
* At least, I copy the war file generated to the folder of **TOMCAT**, and exposed him in port 8080.

    ````RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/````

    ````EXPOSE 8080````

#### Service DB

* First I mentioned, that will be build from an image of ubuntu, then install all the dependencies (java and the wget that will be used to run H2).

    ````FROM ubuntu````

    ````RUN apt-get update && \````

    ````apt-get install -y openjdk-8-jdk-headless && \````

    ````apt-get install unzip -y && \````

    ````apt-get install wget -y````
    
* I create a folder, and copy the jar file throw the website where is H2.

    ````RUN mkdir -p /usr/src/app````

    ````WORKDIR /usr/src/app/````

    ````RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar````

* Then expose the port 8082, to access to H2-console and the 9092, where it will to the connection with the server H2.

    ````EXPOSE 8082````
    ````EXPOSE 9092````
    
* Finally, I execute the program in Java, presented in the jar file of H2.

    ````MD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists````

#### Docker-compose.yml

Now I will defined the two containers(services) that I developed before (Web and H2).

* In each service, I mentioned the folder name that i need to build my image (build: web and build: db).
* Define the mapping of the ports.
* Then defined the dependencies, in this case the web depends of the db, witch means that it will always run first the db.
* Then in the volumes, I mapped the folder that is in my hardDisk with a folder inside the container.

#### Build

Now, I have everything that I need to build my project.

* Open a powerShell, move to the folder ca4, where I have all the files mentioned before.

    * BUILD - It will run the docker-compose.yml file  
       
        ````$ docker-compose build````
    
    * RUN  - Then run the images
        
        ````$ docker-compose up````    
        
As we can see everything went as expected, when I access to web, through the IP of my virtual machine and the pre-defined ports.

#### Web

![Web1](Web1.jpg)

#### DB

![DB1](DB1.jpg)

I made a change in my database where I insert a new employee.
    
* Throw the database I insert:

   ````insert into employee(id,desription,first_name,last_name) values (2,'Ham','Samwise','Gamgee') ```` 


![Web2](Web2.jpg)

So, I need to save my database, in my hardDisk, so If anything happens to my container, the data will not be lost.

* Enter in database

    ````$docker-compose exec db/bin/bash````

* Copy the file to the folder in my hardDisk 

    ````#cp ./jpadb.mv.db /usr/src/data````

![DBSAVE](DB_SAVE.jpg)

Now I will publish the images in DockerHub

I enter in *Docker Quickstart Terminal* as administrator

* Enter in my account of hub.docker that I previous made, and put my data to login.

    ````$ docker login````
    
    ````Username:-----````
    
    ````Password:-----````
    
* List the running containers

    ````$ docker ps -a````
    
* List the running containers

    ````$ docker ps -a````
    
* Commit web
    
    ````$ docker commit f249ba2b7326 devops1050757/web_1````
    
* Push Web
        
    ````$ docker push devops1050757/web_1````  
    
* Commit DB
        
    ````$ docker commit 5c60be37d85c devops1050757/db_1````
        
* Push DB
            
    ````$ docker push devops1050757/db_1````   

![hubdocker](hubdocker.jpg)

### Kubernetes as an alternative to Docker

“Kubernetes vs. Docker” is a somewhat misleading phrase. When you break it down, these words don’t mean what many people intend them to mean, because Docker and Kubernetes aren’t direct competitors. Docker is a containerization platform, 
and Kubernetes is a container orchestrator for container platforms like Docker.  
  
Kubernetes is an open-source container management software developed in the Google platform. It helps using cloud environments.  
  
It is a highly flexible container tool to deliver even complex applications. Applications 'run on clusters of hundreds to thousands of individual servers. 
It also allows us to manage our containerized application more efficiently:  
- Running containers across many different machines   
- Scaling up or down by adding or removing containers when demand changes  
- Keeping storage consistent with multiple instances of an application  
- Distributing load between the containers  
- Launching new containers on different machines if something fails  
There are two basic concepts worth knowing about a Kubernetes cluster. 
The first is node. This is a common term for VMs and/or bare-metal servers that Kubernetes manages.   
  
The second term is pod, which is a basic unit of deployment in Kubernetes. A pod is a collection of related Docker containers that need to coexist. 
Coming back to the nodes, there are two types of nodes. One is the Master Node, where the heart of Kubernetes is installed. 
It controls the scheduling of pods across various worker nodes, where our application actually runs. 
The master node’s job is to make sure that the desired state of the cluster is maintained. 

![kubernets](KubernesInto.jpg) 

Docker is commonly used without Kubernetes, in fact this is the norm. While Kubernetes offers many benefits, it is notoriously complex and there are many scenarios where the overhead of spinning up Kubernetes is unnecessary or unwanted.
As Kubernetes is a container orchestrator, it needs a container runtime in order to orchestrate. Kubernetes is most commonly used with Docker, but it can also be used with any container runtime. 

There are several ways to run a Kubernetes cluster locally, like Minikube or Docker-for Windows.
 In general, Minikube is a vbox instance running Linux and docker-daemon pre-installed. This is actually the only option if the machine does not allow enable hyper-V.
Minikube is a mature solution available for all major operating systems. Its main advantage is that it provides a unified way of working with a local Kubernetes cluster regardless of the operating system. It is perfect for people that are using multiple OS machines and have some basic familiarity with Kubernetes and Docker.

### Task 4: Development of Alternative - Kubernetes

Before implementing kubernets, I had to do a few steps.

* Install Kompose (Kompose takes a Docker Compose file and translates it into Kubernetes resources)
 
* Install Kubectl (command line tool for controlling Kubernetes clusters)

* Install minikube (tool that makes it easy to run Kubernetes locally, Minikube runs a single-node Kubernetes cluster inside a Virtual Machine) - This step was necessary because i have Windows 10 Home

* So now I readpat my docker-compose to kubernetes and to my repository images, previous upload on (DockerHub)[https://hub.docker.com/]     
    * Build from the images on my repository.
    * Change the service names;
    * Deleted networks, because i will not need it.      

````
version: '3'
services:
  kubweb:
    image: devops1050757/web_1
    ports:
      - "8080:8080"
    labels:
      kompose.services.type: LoadBalancer
    depends_on:
      - "kubdb"
  kubdb:
    image: devops1050757/db_1
    ports:
      - "8082:8082"
      - "9092:9092"
````   

* Then I tried to used the docker client to deploy this to a Kubernetes cluster running the controller:

````$ docker stack deploy --orchestrator=kubernetes -c docker-compose.yml web````  

But I got an error
````
failed to find a Stack API version
````  

After some research, I found that, VM don't alow the use of the stack.

* So, I will do it manually, using the kompose.

In the folder, that I have the changed docker-compose.yml file:

````
$kompose convert
$kubectl apply -f kubdb-service.yaml,kubweb-service.yaml,kubdb-deployment.yaml,kubweb-deployment.yaml
````  
I expose the external IP to have the access to my web application and db
````
$kubectl expose deployment kubweb --type=NodePort --name=kubweb-779c5b645-8clhs
````  
To verify the IP of my Vm, that in this case is the IP of the VM where I'm running the minikube 

````
$minikube service kubweb-779c5b645-8clhs --url
````  

* Access to the url-DB
![db](DB.jpg)

* Access to the url-web
![ErroTomcat](ErroTomcat.jpg)

I try to understand the problem, and I identify that was related with the static IP previous defined on application.resources of the application;

* Inside the web container  I execute the following command we proceed to update the database IP (using sed) and rebuild the application as well as make it available on tomcat again.
  
  ````$ sed -i -e 's/192.168.33.11/http://192.168.99.101//g' src/main/resources/application.properties ; ./gradlew build ; cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/````
  
  but I can make the host responding correctly.


Finally I use Kubernetes dashboard, that is the official web-based UI for Kubernetes. 
The dashboard provides an overview of the Kubernetes cluster as well as the individual resources running in it.
So from the folder where is the executable of minikube, I run the command:

```` $.\minikube.exe dashboard ````

![Dashboard](kubernetsDashboard.jpg)
  

As we can see Kubernetes are working, I just have a problem to Tomcat, due to the fact, that the application define a static IP, that wasn´t supported
for kubernetes, and I can't change the IP configurations.






Finally put a tag on my repository to accomplished the task.

___  

     
#### End of Ca4