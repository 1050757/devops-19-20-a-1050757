package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {


    //test the constructors - HappyCases

    @Test
    @DisplayName("Test Constructor: HappyCase - getFirstName")
    void getFirstName() {
        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employeeJoana = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(firstName, employeeJoana.getFirstName());
    }

    @Test
    @DisplayName("Test Constructor: HappyCase - getLastName ")
    void getLastName() {
        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employeeJoana = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(lastName, employeeJoana.getLastName());
    }

    @Test
    @DisplayName("Test Constructor: HappyCase - getDescription ")
    void getDescription() {

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employeeJoana = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(description, employeeJoana.getDescription());
    }

    @Test
    @DisplayName("Test Constructor: HappyCase - getJobTitle")
    void getJobTitle() {

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employeeJoana = new Employee(firstName, lastName, description, jobTitle,email);

        assertEquals(jobTitle, employeeJoana.getJobTitle());
    }

    @Test
    @DisplayName("Test Constructor: HappyCase - getEmail")

    void getEmail() {
        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employeeJoana = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(email, employeeJoana.getEmail());
    }

    //test the constructors - Exceptions

    @Test
    @DisplayName("Test Constructor: firstNameEmpty")

    void checkConstructor_EmptyFirstName() {

        //Arrange
        String firstName = "";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the firstName parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: firstNameNull")

    void checkConstructor_FirstNameIsNull() {

        //Arrange
        String firstName = null;
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the firstName parameter hasn't a valid argument", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: lastNameEmpty")

    void checkConstructor_EmptyLastName() {

        //Arrange

        String firstName = "Joana";
        String lastName = "";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the lastName parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: lastNameNull")

    void checkConstructor_LastNameIsNull() {

        //Arrange

        String firstName = "Joana";
        String lastName = null;
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the lastName parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: descriptionEmpty")

    void checkConstructor_EmptyDescription() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the description parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: descriptionNull")

    void checkConstructor_DescriptionNull() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = null;
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the description parameter hasn't a valid argument", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: jobTitleEmpty")

    void checkConstructor_EmptyJobTitle() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "";
        String email = "joana@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the jobTitle parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: jobTitleNull")

    void checkConstructor_JobTitleNull() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = null;
        String email = "joana@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the jobTitle parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: emailEmpty")

    void checkConstructor_EmptyEmail() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email parameter hasn't a valid argument", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: jobTitleNull")

    void checkConstructor_emailNull() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = null;

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email parameter hasn't a valid argument", thrown.getMessage());

    }

    //Test set's methods

    @Test
    @DisplayName("Test Sets methods")
    void checkSets() {

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employee = new Employee(firstName, lastName, description, jobTitle, email);

        employee.setFirstName("João");
        employee.setLastName("Silva");
        employee.setDescription("Dentist");
        employee.setJobTitle("Doctor");
        employee.setEmail("joao@gmail.com");

        assertEquals("João", employee.getFirstName());
        assertEquals("Silva", employee.getLastName());
        assertEquals("Dentist", employee.getDescription());
        assertEquals("Doctor", employee.getJobTitle());
        assertEquals("joao@gmail.com", employee.getEmail());

    }

    //Test setID to simulate merge

    @Test
    @DisplayName("Test Set ID")
    void checkTestSetID() {

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";
        Long id = Long.getLong("Mil quinhentos e vinte e cinco",1525);

        Employee employee = new Employee(firstName, lastName, description, jobTitle, email);

        employee.setId(id);

        assertEquals(id, employee.getId());

    }

    @Test
    @DisplayName("Test Set ID2")
    void checkTestSetID2() {

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";
        Long id = Long.getLong("1252");

        Employee employee = new Employee(firstName, lastName, description, jobTitle, email);

        employee.setId(id);

        assertEquals(id, employee.getId());

    }

    //Check Equals

    @Test
    @DisplayName("Test Equals")
    void checkEquals() {

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employeeJoana = new Employee(firstName, lastName, description, jobTitle, email);
        Employee employeeExpected = new Employee(firstName, lastName, description, jobTitle, email);

        assertTrue(employeeJoana.equals(employeeExpected));
    }

    //test email exceptions

    @Test
    @DisplayName("Test Constructor: email without arroba")

    void checkConstructor_emailWithout_arroba() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana.sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: email without dot")

    void checkConstructor_emailWithout_dot() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapopt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: email without com")

    void checkConstructor_emailWithout_pt() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.p";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }

    @Test
    @DisplayName("Test Constructor: email without text before dot")

    void checkConstructor_emailWithout_textBeforeDot() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "@sapo.pt";

        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }


    @Test
    @DisplayName("Test Constructor: email without Domain")

    void checkConstructor_emailwithoutDomain() {

        //Arrange

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo";


        //Act

        //Assert
        Throwable thrown = assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
        assertEquals("Employee not created due to the fact that the email wasn't correct", thrown.getMessage());

    }

    //check hashCode

    @Test
    @DisplayName("Test HashCode")
    void checkHashCode() {

        String firstName = "Joana";
        String lastName = "Sousa";
        String description = "Electromechanical";
        String jobTitle = "Engineer";
        String email = "joana@sapo.pt";

        Employee employeeAna = new Employee(firstName, lastName, description, jobTitle, email);
        Employee employeeExpected = new Employee(firstName, lastName, description, jobTitle, email);

        assertEquals(employeeAna.hashCode(),employeeExpected.hashCode());
    }
}
