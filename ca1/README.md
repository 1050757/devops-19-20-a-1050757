# Relatório DevOps -  Ca1

## Demonstração de um Cenário de Git Workflow 
___

___
### Descrição da análise, design e implementação dos requisitos
___

O objetivo deste trabalho passa por simular um cenário de *Git Workflow*, em ambiente de trabalho, sendo que neste seria possível criar diferentes *branches*, compartilhando o mesmo repositório remoto entre diferentes *developers*.

Deste modo, foi clonado o repositório, com as devidas ferramentas do IDE IntelliJ, armazenando numa pasta com a seguinte denominação <devops-19-20-a-1050757>.

De seguida, foi criado uma pasta Ca0, com toda a informação necessária (existente na pasta <devops-19-20-a-1050757>), para inicializar os requisitos da tarefa0.

Para o desenvolvimento da tarefa Ca1, foi copiado todo o conteúdo da pasta Ca0, para esta mesma.

De referir, que foi criada também um pasta *.gitignore*, com a lista de tudo que o git deve ignorar.

Resumidamente, foram efetuadas as seguintes tarefas:

1. Análise dos requisitos do cliente;
1. Definição de novas *features* a serem desenvolvidas;
1. Criação de Issues para assegurar a concretização das tarefas (realização de diversos commits a referenciar as respectivas issues);
1. Formação de *branches* para responder a cada uma das *features*;
1. Merge desses *branches* com o *master*, quando já foi desenvolvida uma resposta efectiva;
1. Criação de tags para representar as diferentes versões quando estabilizadas;


* Instalação do git - Não necessário, pois já tinha sido instalado.  
[Git](https://git-scm.com/downloads);


* Verificar a versão do mesmo na linha de comandos

    ````$ git --version````

* Definir as configurações
    * nome
      
        ````$ git config --global user.name "Henrique Silva"````
    * email
      
        ````$ git config --global user.email "1050757.isep.ipp.pt"````

* Verificar as configurações  

    ````$ git config --list````

* Ir para o diretório da pasta Ca1 
 
    ````$ cd 'C:\Users\ferna\Desktop\SWITCH\Switch\Switch\Cadeiras\DEVOPS\Project\devops-19-20-a-1050757\ca1'````

* Primeiramente todos os *commits* foram efetuados no *Master Branch*, certificando através do comando:  

    ````$ git branch````

* Verificar todos os ficheiros na *Staging Area*, e os ficheiros *Untracked*  

    ````$ git status````
                                
 * Adicionar esses ficheiros à *Staging Area* 
 
    ````$ git add."````  

* Fazer *Commit* dos mesmos. Os *commits* podem ter uma mensagem e referenciar a *Issue*  

    ````$ git commit -m "create directories"````  

* Existe também a possibilidade de  fazer o *Commit* através do comando

    ````$ git commit -a -m "create directories"````  

 
* Sendo assim a primeira tarefa é criar o ficheiro README
    * Na linha de comandos  
    
        ````$ echo 'readme.file' >> README.md````
    * fazendo o respectivo *commit* e *push*, de referir que o ficheiro README foi comitado aquando do commit da pasta Ca1.
    
        ````$ git commit -a -m 'add ca1 [issue 3]'````
        
        ````$ git push````

* foi então atribuído a versão inicial através de um tag relativamente a esse mesmo *commit*, usando referência respectivo desse mesmo *commit*
        
     * verificar o comentário do *commit*  
        
        ````$ git log````
     * criação da tag  
        
        ````$ git tag v1.2.0 1a08351````
     * enviar a tag para o repositório remoto  
        
        ````$ git push --tags````
 
    * Iniciar então uma nova feature, com um novo *branch*, chamado *email-field**  
      
        ````$ git branch email-field````
  
    * Ir para esse *branch*  
     
        ````$ git checkout email-field````

     * Verificar o *branch* correcto  
    
       ````$ git branch````  

    * de seguida efetuar todas as modificações no IDE IntelliJ, para o procedimento correto da *feature* tal como a criação do campo email, alterações em todas as pastas necessárias, criação de testes.   
    * de seguida verificar essas mesmas alterações    
     
        ````$ git diff````

     * Fazer o merge com o master  
        
        ````$ git commit -a -m "adding business code(email feature) and the respective tests for employee[issue 5]”"````       
        
        ````$ git push -u origin email-field````

     * Alterar os testes, e o *commit* respectivo de forma a representar o *merge*, no *Bitbucket
     * Ir para o *master branch*    
        
        ````$ git checkout master````
     * Actualizar eventuais alterações  
        
        ````$ git pull origin master````
     * Verificar os ficheiros a serem adicionados à *staging area*  
       
       ````$ git status````
     * Enviar o *commit* com as alterações feitas no *master branch*  
       
       ````$ git commit -a -m "add setmethod in test to work in branches[issue 5]"````
     * Verificar os *branches* que cursaram *merge* com o *master branch*  
       
       ````$ git branch --merged````
     * Fazer o *merge* do email-field com o *master*                                                                        
        
       ````$ git merge email-field ````
     * Enviar o *push* com todas as alterações                                                                                                                                  
       
       ````$ git push origin master````
     * Marcar o tag com esta nova versão  
        
        ````$ git tag v1.3.0 4643bb1```` 
        
        ````$ git push --tags````      
     
      
   * Proceder à inicialização de um novo *branch* para efetuar a correção de eventuais *bugs* no campo de email, com o nome: **fix-invalid-email**
   Assim que todas as todas as alterações estejam efetuadas, com a versão estável, fazer o 
   merge* com o *Master*, criando uma tag, com as alterações à versão anterior.  
    
      ````$ git branch fix-invalid-email```` 
       
      * Ir para o *branch*
        
        ````$ git checkout fix-invalid-email````
      * Verificar que estou no *branch* correcto  
      
        ````$ git branch````
      * Proceder ás alterações no IDE IntelliJ ( para a criação correta do email usei regex email, e lançei as excepções e respectivos testes). 
      * Aquando da alteração, assegurar que a *feature* estava completo e fazer o merge com o master.  
        
        ````$ git commit -a -m “add test to fix email-bug [issue 6]" ````  
        
        ````$ git push -u origin fix-invalid-email ````
      * para representar o merge visualmente no Bitbucket, efetuar uma pequena alteração no IDE aos testes, e à *DatabaseLoader*, no *MasterBranch*                                                                                                                                                                                                                                                                                     
       Fazer o *commit* dos mesmos
      * Ir para o *master branch*  
        
        ````$ git checkout master````
      * Verificar as eventuais alterações  
        
        ````$ git pull origin master````
      * Verifiqar os ficheiros a serem adicionados à *staging area*  
       
        ````$ git status````
      * Fazer o respetivo *commit*  
        
        ````$ git commit -a -m "add more employees to repository"````
      * Confirmar os *branches* que efetuaram *merge* com o *master branch*  
        
        ````$ git branch --merged ````
      * Fazer o *merge* do fix-invalid-email com o *master*  
        
        ````$ git merge fix-invalid-email````
      * Efetuar o *push* de todas as alterações                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
        
        ````$ git push origin master````
      * Para finalizar criar uma tag para esta nova versão  
      
        ````$ git tag v1.3.1 ed3a743 ````    
        ````$ git push --tags````  
  
                                                    
                                                                
  * Eliminar então os *branches* utilizados não necessários (este passo foi já efetuado):
    * Verificar os *branches* que já foram *merged*  
        
        ````$ git branch --merged````    
    * Eliminar os diferentes *branches*
        * Eliminei o *branch* local  
            
            ````$ git branch -d email-field````
        * Eliminar o *branch* remoto                                         
            
            ````$ git push origin --delete email-field````
        * Eliminar o *branch* local                                                                                   
            
            ````$ git branch -d fix-invalid-email````
        * Eliminar do *branch* remoto                                                                                                                      
            
            ````$ git push origin --delete fix-invalid-email````                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
___
Represento aqui a minha imagem geral no Bitbucket no final
![Bitbucket-Commits e Branches](Bitbucket.jpeg)

#### Notas:
Na imagem do bitbucket não aparece o commit relativamente ao ficheiro README.md, pois será efetuado após a finalização do mesmo.
Após o término fiz fix de todas as issues, menos a que criei para o .gitignore.
Erradamente os meus commits foram referencidos como [issue n], sendo que a maneira correta seria ref #n.

## Mercurial como alternativa ao git

    
O Mercurial é uma ferramenta de controle de versões distribuída e gratuita. Suporta vários sistemas operacionais tais como Windows, Linux, Mac OS, etc. É capaz de lidar com qualquer projeto. Visto que cada clone consiste no histórico do projeto, e a maioria das ações é rápida isto permite o “developer” usar extensões. Dado que existe mais documentação relativamente ao Mercurial, isto permite ao usuário uma melhor aprendizagem, além do mais outros Sistemas de Controle de Versões como o SVS podem ser facilmente adaptados a Mercurial.

Além da linha de comandos o Mercurial possui uma extensão gráfica da interface do usuário. No geral, o Mercurial fornece alto desempenho e escalabilidade, sendo este capaz de gerenciar também texto simples e arquivos binários.

Relativamente às principais diferenças entre GIT e Mercurial, são várias, tais como:
- O GIT caracteriza-se, por ser um Sistema de Controle de Versão Distribuído para rastrear alterações no código-fonte durante o desenvolvimento do software, ao passo que o Mercurial é uma ferramenta de Controle de Versão DIstribuído para “developers”;
-O criador do GIT, foi Linus Torvalds, sendo que Junio Hamano é o atual desenvolvedor do GIT. Matt Mackall foi o criador do Mercurial.
- O GIT é desenvolvido em C, Shell, Perl, Tcl e Python, e o Mercurial é desenvolvido em C e Python.
- Em termos de editação do histórico existem diferenças significativas. Assim, o GIT possibilita ao "developer" a sua alteração, enquanto que o mercurial não apresenta essa particularidade (apenas permite a alteração da ultima confirmação). 
- O Mercurial não existe uma noção de “Staging Area”.
- O GIT concede uma melhor ramificação que Mercurial.
Deste modo, podemos concluir que o Mercurial é mais intuitivo o que permite uma melhor adaptação, por parte de “developers” menos experientes, e por uma comunidade menos técnica. O GIT é uma ferramenta mais avançada e usada na indústria de desenvolvimento de software.