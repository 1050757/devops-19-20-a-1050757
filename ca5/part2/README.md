# Ca5-Part2  
___
## Pipeline to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2)  
* The main goal is to work from our previous work developed in ca2/part2 directory.
 This use Spring Dat Rest and its powerful backend functionality, combined with React’s sophisticated features to build an easy-to-understand UI. It was an 
 adaptation of the basic directory of CA1, with Gradle spring project with all the necessary dependencies ( Rest Repositories, Thymeleaf, JPA, H2). 
  
* The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2);  


#### Main Goals
##### Stages of the pipeline
* **Checkout**: To checkout the code from the repository;  
* **Assemble**: Compiles and Produces the archive files with the application;  
* **Javadoc**: Generates the javadoc of the project and publish it in Jenkins;  
* **Archive**: Archives in Jenkins the archive files (generated during Assemble, i.e., the war file);  
* **Publish Image**: Generate a docker image with Tomcat and the war file and publish it in the Docker Hub;

## Development

* I start to create a Readme file in a new directory ca5/part2 (ref #34):   
````$ cd C:\Users\ferna\Desktop\SWITCH\Switch\Switch\Cadeiras\DEVOPS\Project\devops-19-20-a-1050757\ca5````  
````$ mkdir part2````  
````$ cd part2````  
````$ $ echo README >> README.md````  
````$ Readme and jenkinsfile ref #34"````    
````$ git push```` 

* In my folder ca2/part2 I create some tests.

#### Create a Job in Jenkins
* I start a new Job in Jenkins. 
 
* Click in New Item. Give the new item a name:   
    * Defines the Job name: **devops1050757ca5part2**;  
    
    * Defines the type as a Pipeline;  
    
    * Here we can see that I already have two jobs in my Jenkins;
        ![Item](Item.jpg)  
        

 #### Configuration of the Pipeline in Jenkins  
  * Definition:  
````Pipeline script from SCM````  
  
  * SCM:  
````Git````  
  
  * Repository URL:  
````https://1050757@bitbucket.org/1050757/devops-19-20-a-1050757````  
  
  * Script Path:  
````ca2/part2/demo/Jenkinsfile```` 

![definition](definition.jpg)   

#### Develop Pipeline as a code (Jenkinsfile)  
* I start to create a Jenkinsfile in the directory where I have the spring boot application(ca2/part2):  
````$ cd C:\Users\ferna\Desktop\SWITCH\Switch\Switch\Cadeiras\DEVOPS\Project\devops-19-20-a-1050757\ca2\part2\demo```` 
 
````$ echo Jenkinsfile >> Jenkinsfile````  

* All the script and changes to the Pipeline will be made in this file.

````  
pipeline {
    agent any
````

* Stage to checkout the code from the repository:

````
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://bitbucket.org/1050757/devops-19-20-a-1050757.git'
            }
        }
```` 
* Stage to compile and produces the archive files with the application: 
````
    stage('Assemble') {
        steps {
            dir("ca2/part2/demo"){
                   echo 'Assemble...'
                   bat 'gradlew clean build --rerun-tasks -x test'
            }
        }
    }
````
* Create a test stage that will execute the project's tests;
````
    stage('Test') {
         steps {
             dir("ca2/part2/demo"){
                 echo 'Testing...'
                 bat 'gradlew test'
             }
         }
    }
````   
* A stage to generate Javadoc to publish it in Jenkins;  
Archive/publish html reports; 
````
stage('Javadoc') {
	steps {
	    dir("ca2/part2/demo") {					
		echo 'Generating the Javadoc documentation...'
		bat 'gradlew javadoc'
		publishHTML(target : [allowMissing: false,
		alwaysLinkToLastBuild: true,
		keepAll: true,
		reportDir: './',
		reportFiles: 'myreport.html',
		reportName: 'My Reports',
		reportTitles: 'The Report'])
		}
	}
}
````
* A stage to archive the deployment files, who will be generated during the first stage.  
This will archive a jar file and a war file, that was generated during the Assemble task;
````
stage('Archiving') {
    steps {
        dir("ca2/part2/demo") {
	    echo 'Archiving...'
            archiveArtifacts 'build/libs/*'
		}
	}
}
````
  * Generate a docker image with Tomcat and the war file:  
  * And the push the image in the Docker Hub.
````  
stage('Push image') {
    steps {
    dir("ca2/part2/demo") {
        script {
        docker.withRegistry('https://registry.hub.docker.com', 'docker-devops1050757ca5part2') {
        def customImage = docker.build("devops1050757/my-image:${env.BUILD_ID}")
        customImage.push()
	    }
	}
    }
}

````
*  docker.build("devops1050757/myimage:${env.BUILD_ID}"):
    * here I need to indicate the name of my DockerHubID: *"devops1050757"*;  
    * here I define that the tag that is the number of my Jenkins build: *"{env.BUILD_ID}"*;   
* Then I make the push of the created image;

* I have some problems with my credentials, so I made a change in the config.json file in my docker.

* Now I will create a dockerfile to generate a docker image, with the commands necessary to create a dockerImage:

````
FROM tomcat


RUN apt-get update -y

RUN apt-get install tomcat9-admin -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN apt-get install nano -y

RUN mkdir -p /tmp/build

RUN mkdir -p /usr/local/tomcat/webapps/ROOT

WORKDIR /tmp/build/

COPY build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/demo-0.0.1-SNAPSHOT.war

RUN chmod -R 777 /usr/local/tomcat/webapps/

EXPOSE 8080
````

* Here we can see that the image was upload with success. 

![dockerHub](dockerHub.jpg)

* Now I will publish the test results;  
* Post: defines one or more additional steps that are run after the pipeline finishes. 

````
post {
     always {
	echo 'Result of our tests!!'
        junit '**/test-results/test/*.xml'
		}
	}
} 
````

* Here we can the see the whole pipeline.

![jenkinsBuild](jenkinsBuild.jpg)

* To finished my task, I commit the dockerfile and the jenkinsfile and put a tag ca5-part2.

___        
##### End of Ca5-Part2