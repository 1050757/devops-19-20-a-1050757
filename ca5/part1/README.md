# The implementation CI/CD pipeline - Basic demo Project
___

  
### Meeting Jenkins  

* Continuous integration (CI) as the name suggests, it's a coding philosophy, with a set of pratices that drive development teams to implement small changes 
and check in code to version control repositories frequently.   
His technical goal is to establish a consistent and automated way to build, package and test applications.

* Continuous delivery (CD) appear where the continous integration ends, it automates the delivery of applications to selected infrastructure environments.    
It's a way to automate the code changes made by teams, who work with multiple environments other than the production, such as development and testing.

* So, Jenkins offers a simples way to set up a CI/CD environment for almost any combination of languages and source code repositories using pipelines and other routine development tasks.    
It gives a faster and more robust way to integrate the entire chain of build, test and deployment tools, but doesn't eliminate the need to create scripts for individual steps.    

* Contextualizing, we can say that there were a before and a after Jenkis, where the big differences are the fact that after jenkins, every commit in the source code is built and test,
 witch give a powerful tool to developers, because they know the result of every test.   It's a time saver, and automating tool.    

* But like everything, it has some flaws:
  * Not a friendly interface, compare to current UI trends.
  * It's not easy to mantain, it runs in a server and requires skills as server administrator to monitor its activity.
  * It's not easy to install and configure, witch keep people away.
  * Continous integrations regularly break due to some small setting changes.
  * Continuous integration will be pause and therefore requires some developer attention.

* It was developed a new plugin, Blue Ocean, witch gives a new experience to the user, 
 modelling and presenting the process of software delivery by surfacing information that's important to development teams with as few clicks as possible, 
 while still staying true to the extensibiliy that is core to Jenkins.  
 

___  
### Development  

* First I create a new directory for task **Ca5** and a **part1**  

    ````$ mkdir ca5````  
    ````$ mkdir part1```` 
       
* Create a readme file in the directory: 
     
    ````$ echo readme >> README.md````   
    ````$ git commit -m "create README.md ref#32"````   
    ````$ git push````  

* Installing Jenkins:
   
 * Download stable Jenkins war, from the website.
 
 * To avoid conflicts, I use the port 9090, because I have other project running in port 8080. I have to run this from the directory where I have the war file.  
        ````$ java -jar jenkins.war --httpPort=9090```` 

 * Insert the password generated.       
   
 * Install some pluggins.  
  
 * Define everything to start use Jenkins (username, password, name, and email).  
  
 * Start a new Job for the project.  
  
 * Define the Job name: **devops1050757**.  

 * Define the type pipeline (declarative).  
   
 * Then configure my job.
  
It's important to mention that I will use a script from SCM. I will have the Jenkisfile in the root of my project, **ca2/part1**, that I will use
to define all the Job stages.  
  
* Pipeline in the browser:  
  * Definition:  
````Pipeline script from SCM````  
  
  * SCM:  
````Git````  
  
  * Repository URL:  
````https://1050757@bitbucket.org/1050757/devops-19-20-a-1050757````  
  
  * Script Path:  
````ca2/part1/Jenkinsfile````  
  
![Pipeline](pipeline.jpg)    
              
 * Now I can start to create my JenkinsFile. In the root of ca2/part1, I generate and edit it:     
   
   * Starting a new pipeline:  
````  
pipeline {
    agent any
        stages {
````
   
   * Stage to checkout the code from the repository:  
````
        stage('Checkout') {
                    steps {
                        echo 'Checking out...'
                        git url: 'https://bitbucket.org/1050757/devops-19-20-a-1050757/src/master/ca2/part1'
                    }
                }
````  

   * Stage to compile and produces the archive files with the application:  
````
        stage('Assemble') {
                    steps {
                       dir("ca2/part1"){
                           echo 'Assemble...'
                           bat 'gradlew clean build --rerun-tasks -x test'
                       }
                     }
                }
````  

   * Stage test, to executes the tests:  
````
        stage('Test') {
                     steps {
                        dir("ca2/part1"){
                            echo 'Testing...'
                            bat 'gradlew test'
                         }
                      }
                 }
````  

   * Stage to archives in Jenkins the archive files, generated during the first stage; (this finish the stages):
````
        stage('Archiving') {
                    steps {
                        echo 'Archiving...'
                        archiveArtifacts 'ca2/part1/build/distributions/*'
                    }
                }
        
           } /* Stages*/
````  

   * Post stages to publish the test results:  
````
   post {
         always {
           junit '**/test-results/test/*.xml'
         }
      }
   
   } /* pipeline */
````            

It's important to mention that I have made some mistakes at first, especially with redirections, and that's why, there are some build's in my jenkins.
Another problem was with the encoding, so I have to open the JenkinsFile with the notepad, and convert to UTF-8 without BOM.

* It's now time to commit the jenkisFile for the **ca2/part1**, to then make the manual build of the Jenkins browser:   
 
````$ git commit -m "Create a jenkinsFile ref #33"````  

During all this process, the Jenkins must be running in the PowerShell.  

   * As we can see in the image below the build occurred successfully. 
  
![Build](JenkinsBuild1.jpg)  
       
   * In the workspace is possible to see the files, and prove that everything went as expected. 
     
![workSpace](Workspace1.jpg)  
  
   * And the test results  
  
![Tests](JenkinsTestPassed1.jpg)  

   * Then I download the plugin **BlueOcean**. Here we can see the same results, but with a more friendly UI.  
  
![OceanBlue](OceanBlue.jpg)  
                   
___        
##### End of Ca5-Part1 
