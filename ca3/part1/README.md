Ca3 - Part1 - VirtualBox with Ubuntu
___
### Introduction to Virtual Machine

We can say that a virtual machine simulates an operating system.  
So, we can "create" a computer inside of and another computer.
We have a lot of reasons to use the virtualization:
* no need to buy more hard drives or hard disks.
* when we work inside ot the VM, we do not interfere with the rest of the computer, and because of that they are good to test new applications or setting up a production environment.
* It's possible to place many virtual servers onto each physical server to improve hardware utilization.

Therefore, to complete our task, i will use the hypervisor that was used was VirtualBox.  
VirtualBox is a typeII hypervisor. This type of hypervisor requires a host OS to be run on.

### Task 3 - Part1: Development
___

* I installed the new VM, in virtualBox with the operation system Linux (Ubuntu), that i will not explain here
**(Target 1)**
* I started to open a Windows powerShell
    * went to my repository:  
    ````$ cd C:\Users\ferna\Desktop\SWITCH\Switch\Switch\Cadeiras\DEVOPS\Project\devops-19-20-a-1050757````  
    * create a directory to the new task  
    ````$ mkdir ca3````
    * create other directory to the new task  
    ````$ mkdir part1````  
    * create a README file  
    ````$ echo README >> README.md"````  
    * start the Issue in Bitbucket : *Issue 17*  
    * confirm that I have changes to commit 
    ````$ git status````
    * they were in the untracked, so I add.
    ````$ git add ca3````  
    * commit those changes and push them to the remote repository
    ````$ git commit -a -m "create the directory ca3 and a README file for task Ca3-Part1 ref#17"````  
    ````$ git push````  
   
So now i have to install all the dependencies that i will need, like:

    * GIT  
        ````$ sudo apt install git````
    * JDK  
        ````$ sudo apt install openjdk-8-jdk-headless````
    * Maven  
        ````$ sudo apt install maven````
    * Gradle  
        ````$ sudo apt install gradle````  


* Then I install the openssh-server, changing the password authentication for ssh, and install an ftp server, and enable the write acess.
* so when in the powerShell, I connect to the VM, as we can see in the image below
![ssh](ssh.jpg)

**(Target 2)**

* Clone my individual repository inside the VM  
   ````$ git clone https://1050757@bitbucket.org/1050757/devops-19-20-a-1050757.git```` 
   
![Git clone my repository](VB.jpg)

**(Target 3)**

* So now I confirmed if the clone has been executed currently  
    ````$ ls ````  
* Move to the directory that was cloned:  
    ````$ cd devops-19-20-a-1050757```` 
* Went to the **basic** directory, where is the spring boot tutorial basic project:  
    ````$ cd ca1\basic````  
    * Because of some permissions, I had to alter the mvnw file  
    ````$ chmod 777 mvnw````
    * And the executed the basic spring boot tutorial:  
    ````$ ./mvnw Spring-boot: run````
    * then in my host computer's browser google chrome I open the application with the URL:
    ```` http://192.168.56.6:8080/````
    * Everything went as expected, as It's possible to see in the following picture:  
    ![Spring Application](SpringApplication.jpg)  
    
    **(Target 4)**

* Next I changed to the directory where I have my Gradle project:  
    ````$ cd devops-10-20-a-1050757/ca2/part1````
    * I listed my files and directories to check permissions, and once again I had to change the permission of the gradlew file:    
    ````$ chmod 777 gradlew````
    * I execute the gradle project:  
    ````$ ./gradlew build````  
    * And I run the server in my VM:  
    ```` ./gradlew runServer````  

* Then I change the groovy for the **runClient**, deleted the args  

```groovy
       task runClient(type:JavaExec, dependsOn: classes){
           group = "DevOps"
           description = "Launches a chat client that connects to a server on localhost:59001 "
       
           classpath = sourceSets.main.runtimeClasspath
       
           main = 'basic_demo.ChatClientApp'       
           
       }
```
    
* To run the client, task **runClient** we must use a different command line, as I used powerShell.  
     * I open the directory in the powerShell:  
        ````$ cd C:\Users\ferna\Desktop\SWITCH\Switch\Switch\Cadeiras\DEVOPS\Project\devops-19-20-a-1050757\ca3\part1````
     * Execute the Gradle **build** task:  
        ````$ ./gradlew build````  
     * Run the **runClient** task:  
        ````$ ../gradlew runClient --args '192.168.56.6 59001````  
     
* I repeated the same for open a new Client as we can see in the image below, and everything works as expected.
![RunClient](gradlew.jpg)
    **(Target 5)**

___       
#### End of Ca3-Part1  

