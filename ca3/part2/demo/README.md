Ca3 - Part2 - Vagrant
___
### Introduction to Vagrant

Vagrant is used to automate the setup of one or more VMs, and it can be use with a simple configuration file.
It can be used in several operating systems with different providers, and the great advantage is that it can easy reproduce environments.

Vagrant give important advantages like isolation, sharable and reproducible environment, support different hardware and OS.

### Task 3 - Part2: Development

So for this task we will use a vagrantfile that it will generate the 2 VMs, where one is web, that is used to run tomcat 
and the spring application and the other is a db, who will execute the H2 server database.

* First I copy the vagrantfile to my repository in a folder named ca3 part2.

* I made a change in the vagrantfile, so it used my own gradle version of the spring application.
![vagrantFile](VagrantFile.jpg) 

* Next, I copy my previous project developed in the Class Assigment 2, part 2 - Build Tools with Gradle, to my folder ca3 part 2.

* Now, i will make some changes to work in this scenario

    * ADD: Suport for building war file
    * ADD: Support for h2 console
    * ADD: weballowothers to h2
    * ADD: Application context path
    * UPDATE: Fixes ref to css in index.html
    * ADD: Settings for remote h2 database
    * UPDATE: Fixes h2 connection string
    * UPDATE: So that spring will no drop de database on every execution
    
* Now in the powerShell, I will run the 2 VMs

    ```` vagrant reload provision````
    
    ```` vagrant up````

As we can see in the image below, they are both running now.
    
![VMs](VMs.jpg)
     
* So now I am lunch the apllication with the ./gradlew clean build

* Then I went to the web, and as we can see everything run as expected, except I made some mistakes before
so I run it 3 times and the result have repeat 3 times.

![LocalHost](LocalHost.jpg)

* Web

![Web](Web.jpg)

* DB

![LocalHost](DB_Test.jpg)

![LocalHost](DB.jpg)

* At last, I made a tag in bitbucket to finished the ca2 part2.

 
___
### Hyper-V as an Alternative to VirtualBox

* So now, I have to find an alternative for virtualBox, and i decided to use Hyper-V, witch is a native hypervisor developed by microsoft.
    * Hyper-V is available by default on almost Windows 8.1 and later versions (I have windows 10 Pro).
    
* By default Hyper-v doesn't come enable, so i have to do it manually.
    * Went to programs in Control panel
    * Select programs and features.
    * Went to turn windows features on or off
    * Enable Hyper-V by put a visa, and then click ok, and restart the system.
    * Now, I can have acesso to the hyper-v manager as an administrator.
    
### Implementation of the Alternative

So now, I have to update my vagrant.file so I can executed it, I had to choose the Vagrant Box that I would use in the Vagrant configuration file. 
I decided to use a similar to the one previously used:**"generic/ubuntu1804"**, but there were other options.     

* Now I have to download the vagrant box:  
````$ vagrant box add generic/ubuntu1804 --provider hyperv````  
* Then I crate a folder inside of ca3-part2, as alternative and I copy the previous vagrant file.
* It's necessary to readapt the vagrant file to the provider: HyperV.  
    * Change the configurations to the Vagrant Box, ("envimation/ubuntu-xenial" doesn't support Hyper-V):  
    ````config.vm.box = "generic/ubuntu1804"```` - the global configuration   
    ````db.vm.box = "generic/ubuntu1804"```` - configurantion of the db VM  
    ````web.vm.box = "generic/ubuntu1804"```` - configuration of the web VM  
    * Updated the provider:  
    ````db.vm.provider "hyperv" do |v|```` - for the db VM  
    ````web.vm.provider "hyperv" do |v|```` - for the web VM  
    * Change the memory of my VM´s, so I had to change that definition on the VagrantFile:  
    ````v.memory = 1024```` - for both of my VM's  
    * Make some changes when I clone my remote repository  
        * clone my repository:  
        ````git clone https://1050757@bitbucket.org/1050757/devops-19-20-a-1050757.git````  
        * open where my Gradle executable is:    
        ````cd devops-19-20-a-1050757/ca3/part2/demo````  
        * I have to ensure that previous node and node_modules are removed before the build:  
         ```` rm -rf node```` 
         ```` rm -rf node_modules````  
        * change the gradlew permissions:      
        ````chmod 777 gradlew````  
* At last, I provisioned the VM's(web and db):  
````$ vagrant up --provider hyperv````  
In the end we can see that both VMs are installed and running  
![VMS](VMsHyper.jpg)   
\
* It's also need to update the application.properties file, to directed the definition of the spring.datasource to the db's IP:
```` vagrant ssh web````  
     * Acesso and edit the file application.properties manually:
      ```` sudo nano application.properties````  
     * Then I change the direction to the ip of my db. 
     * Then I ran the build, that generated the apllication's war file:  
     ```` ./gradlew clean buid````
     * To deploy the war file to tomcat8 I had to run the following command:
      ````sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps/basic-0.0.1-SNAPSHOT.war````  
     * After that I restarted the Tomcat8 service:  
     ```` sudo service tomcat8 stop````   
     ```` sudo service tomcat8 start````   
     
* To accomplished my task I acess to the web application, and database.    
      
#### End of Ca3-Part2 and alternative
