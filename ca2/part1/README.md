# Class Assigment 2 - Part 1

### Creating my project
1. Created folder ca2-part1 in my repository
2. Than downloaded the [project for this assigment](https://bitbucket.org/luisnogueira/gradle_basic_demo/)

3. Imported the project to IntelliJ using the file build.gradle present in the folder of the project

### Following the README file instructions
4. Built the project  
`gradlew build`
5. Run the server  
`java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001`
6. Run a client  
`gradlew runClient`

This allowed me to create a chat only in my computer.![Imagem *build*](build.jpg)

In case, as an example, if i wanna to communicate with another server, it will be necessary to go to the file build.gradle and changed the "localhost" int the task runClient to the ip necessary.


## Creation of the *runServer* task in Gradle
1. Access to the build.gradle file
2. Produce the task named runServer based on the runClient task  
```
task runServer(type: JavaExec, dependsOn: classes) {

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}
```

## Testing the App
1. Created a folder to AppTest file, and than generated in the class
> src/test/java/basic_demo/AppTest.java
2. Created a unit test 
```
@Test
public class AppTest {
    @Test public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
```
3. Add the dependency in the build.gradle file, to be able to run successfully `gradlew build` and `gradlew test`  
```
depedencies {
     // Test dependency
     testCompile 'junit:junit:4.12'
}
```

## Creation of the *copy* task in Gradle
1. Access to the build.gradle file
2. Produced a task named copyTaskBackupSrc of the type Copy  
```
task copyTaskBackupSrc(type: Copy) {
    from 'src'
    into 'backup_src'
}
```

## Creation of the *zip* task in Gradle
1. Access to the build.gradle file
2. Created a task named zipSrc of the type Zip  
```
task zipSrc(type: Zip) {
    archiveFileName = "zip_src.zip"
    destinationDirectory = file("zip_src")


    from "src"
}
```
 